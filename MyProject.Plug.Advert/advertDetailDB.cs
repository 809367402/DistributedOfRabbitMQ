using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using RabbitMQ.Client;
using OSGH.DataAccess.Advert;
using MyProject.DataEntity;

namespace OSGH.Plug.Advert
{
    public class advertDetailDB : BaseMessageConsumer
    {
        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header, ILog loger)
        {
            advertDetail detail = new advertDetail();
            object obj = null;
            try
            {
                obj = base.ConvertFromMessageToObject(message);
                AccessParam acc = obj as AccessParam;
                loger.Debug(ToJson(obj));
                if (string.IsNullOrEmpty(acc.MsgBelongCinema) || acc.MsgBelongCinema.Equals(CinemaHelper.cinameCode))
                {
                    switch (acc.MessageMethodName)
                    {
                        case EnumMessageMethodcs.Exists:
                            detail.Exists(obj);
                            break;
                        case EnumMessageMethodcs.Add:
                            detail.Add(obj);
                            break;
                        case EnumMessageMethodcs.Update:
                            detail.Update(obj);
                            break;
                        case EnumMessageMethodcs.Delete:
                            detail.Delete(obj);
                            break;
                        case EnumMessageMethodcs.DeleteList:
                            detail.DeleteList(obj);
                            break;
                        case EnumMessageMethodcs.GetModel:
                            detail.GetModel(obj);
                            break;
                        case EnumMessageMethodcs.GetSource:
                            detail.GetSource(obj);
                            break;
                        case EnumMessageMethodcs.GetListEntity:
                            detail.GetListEntity(obj);
                            break;
                    }
                    channel.BasicAck(header.DeliveryTag, false);
                }
                else
                {
                    channel.BasicAck(header.DeliveryTag, true);
                }
            }
            catch (Exception ex)
            {
                channel.BasicAck(header.DeliveryTag, true);
                LogAccessParm(obj);
                loger.Error(ex);
            }
        }
    }
}


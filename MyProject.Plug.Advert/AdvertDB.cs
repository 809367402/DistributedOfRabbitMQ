using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using RabbitMQ.Client;
using MyProject.DataEntity;

namespace OSGH.Plug.Advert
{
    public class AdvertDB : BaseMessageConsumer
    {
        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header, ILog loger)
        {
            object obj = null;
            try
            {
                obj = base.ConvertFromMessageToObject(message);
                loger.Debug(ToJson(obj));
                AccessParam acc = obj as AccessParam;
                DataAccess.Advert.Advert advert = new DataAccess.Advert.Advert();
                if (string.IsNullOrEmpty(acc.MsgBelongCinema)||acc.MsgBelongCinema.Equals(CinemaHelper.cinameCode))
                {
                    switch (acc.MessageMethodName)
                    {
                        case EnumMessageMethodcs.Exists:
                            advert.Exists(obj);
                            break;
                        case EnumMessageMethodcs.Add:
                            advert.Add(obj);
                            break;
                        case EnumMessageMethodcs.Update:
                            advert.Update(obj);
                            break;
                        case EnumMessageMethodcs.Delete:
                            advert.Delete(obj);
                            break;
                        case EnumMessageMethodcs.DeleteList:
                            advert.DeleteList(obj);
                            break;
                        case EnumMessageMethodcs.GetModel:
                            advert.GetModel(obj);
                            break;
                        case EnumMessageMethodcs.GetSource:
                            advert.GetSource(obj);
                            break;
                        case EnumMessageMethodcs.GetListEntity:
                            advert.GetListEntity(obj);
                            break;
                    }
                    channel.BasicAck(header.DeliveryTag, false);
                }
                else
                {
                    channel.BasicAck(header.DeliveryTag, true);
                }
            }
            catch (Exception ex)
            {
                channel.BasicAck(header.DeliveryTag, true);
                LogAccessParm(ToJson(obj));
                loger.Debug(ex);
            }
        }
    }
}

using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RabbitMQ.Client;
using log4net;
using MyProject.DataEntity;
using OSGH.DataAccess.Advert;

namespace OSGH.Plug.Advert
{
    public class AdvertRuleDB : BaseMessageConsumer
    {
        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header, ILog loger)
        {
            AdvertRule rule = new AdvertRule();
            object obj = null;
            try
            {
                obj = base.ConvertFromMessageToObject(message);
                loger.Debug(ToJson(obj));
                AccessParam acc = obj as AccessParam;
                if (string.IsNullOrEmpty(acc.MsgBelongCinema)||acc.MsgBelongCinema.Equals(CinemaHelper.cinameCode))
                {
                    switch (acc.MessageMethodName)
                    {
                        case EnumMessageMethodcs.Exists:
                            rule.Exists(obj);
                            break;
                        case EnumMessageMethodcs.Add:
                            rule.Add(obj);
                            break;
                        case EnumMessageMethodcs.Update:
                            rule.Update(obj);
                            break;
                        case EnumMessageMethodcs.Delete:
                            rule.Delete(obj);
                            break;
                        case EnumMessageMethodcs.DeleteList:
                            rule.DeleteList(obj);
                            break;
                        case EnumMessageMethodcs.GetModel:
                            rule.GetModel(obj);
                            break;
                        case EnumMessageMethodcs.GetSource:
                            rule.GetSource(obj);
                            break;
                        case EnumMessageMethodcs.GetListEntity:
                            rule.GetListEntity(obj);
                            break;
                    }
                    channel.BasicAck(header.DeliveryTag, false);
                }
                else
                {
                    channel.BasicAck(header.DeliveryTag, true);
                }
            }
            catch (Exception ex)
            {
                channel.BasicAck(header.DeliveryTag, true);
                LogAccessParm(obj);
                loger.Error(ex);
            }
        }
    }
}

﻿using log4net;
using RabbitMQ.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace MyProject.HQ.Consume.Services
{
    public partial class Service1 : ServiceBase
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Service1));
        private double Time_ProtectInterval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["ConsumerProtectInterval"]);

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.timerComsumer.Enabled = true;
            this.timerComsumer.Interval = Time_ProtectInterval * 60 * 1000;
            this.timerComsumer.Start();
            try
            {
                RQConsumer.ConsumerServiceStart();
                log.Info("......总部端同步服务启动......");
            }
            catch (Exception ex)
            {
                log.Error("总部消费者服务启动失败系统异常，详细信息如下");
                log.Error(ex.ToString());
            }
        }

        protected override void OnStop()
        {
            this.timerComsumer.Enabled = false;
            this.timerComsumer.Stop();
            log.Info("......总部同步服务关闭......");
        }

        bool IsCheckComplate = true;
        int Times = 0;
        private void timerComsumer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            if (IsCheckComplate)
            {
                IsCheckComplate = false;

                Times += 1;
                log.InfoFormat("......影院同步服务消费者订阅第{0}次......", Times.ToString());

                RQConsumer.ConsumerServiceStart();
            }
            IsCheckComplate = true;
        }
    }
}

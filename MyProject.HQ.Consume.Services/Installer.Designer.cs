﻿namespace MyProject.HQ.Consume.Services
{
    partial class Installer
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.CYCOMPHQDSServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.OSGHHQDSProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // CYCOMPHQDSServiceInstaller
            // 
            this.CYCOMPHQDSServiceInstaller.Description = "CYCOMP总部端数据同步服务";
            this.CYCOMPHQDSServiceInstaller.DisplayName = "MyProject HQ Data Synchronism Service";
            this.CYCOMPHQDSServiceInstaller.ServiceName = "CYCOMPHQDSService";
            this.CYCOMPHQDSServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // OSGHHQDSProcessInstaller
            // 
            this.OSGHHQDSProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.OSGHHQDSProcessInstaller.Password = null;
            this.OSGHHQDSProcessInstaller.Username = null;
            // 
            // Installer
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CYCOMPHQDSServiceInstaller,
            this.OSGHHQDSProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceInstaller CYCOMPHQDSServiceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller OSGHHQDSProcessInstaller;
    }
}
namespace MyProject.Consume.Services
{
    partial class Installer
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.OSGHCinemasDSServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.OSGHCinemasDSProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            // 
            // OSGHCinemasDSServiceInstaller
            // 
            this.OSGHCinemasDSServiceInstaller.Description = "CYCOMP影院端数据同步服务";
            this.OSGHCinemasDSServiceInstaller.DisplayName = "MyProject Data Synchronism Service";
            this.OSGHCinemasDSServiceInstaller.ServiceName = "MyProjectCinemasDSService";
            this.OSGHCinemasDSServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // OSGHCinemasDSProcessInstaller
            // 
            this.OSGHCinemasDSProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.OSGHCinemasDSProcessInstaller.Password = null;
            this.OSGHCinemasDSProcessInstaller.Username = null;
            // 
            // Installer
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.OSGHCinemasDSProcessInstaller,
            this.OSGHCinemasDSServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceInstaller OSGHCinemasDSServiceInstaller;
        private System.ServiceProcess.ServiceProcessInstaller OSGHCinemasDSProcessInstaller;
    }
}
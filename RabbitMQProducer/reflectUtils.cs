﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RabbitMQDemo
{
    public class reflectUtils
    {
        /// <summary>
        /// 动态创建指定的类
        /// <remarks>该方法可以代替原来DALFactory中的各个数据访问接口的定义</remarks>
        /// </summary>
        /// <param name="sKey">配置文件中程序集的定义“键值”</param>
        /// <param name="sClass">数据类名</param>
        /// <returns></returns>
        internal static object Create(string sPath, string sClass)
        {
            Assembly AssemblyDAL;
            AssemblyDAL = Assembly.Load(sPath);

            sClass = sPath + "." + sClass;
            object oEventBatch = AssemblyDAL.CreateInstance(sClass, true);

            return oEventBatch;
        }

         
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace RabbitMQDemo
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\log4net.config"));


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());//(new Form3());//
            Application.Run(new Form3());

        }


        //static void Main()
        //{
        //    RabbitMQueue.Send("this is a rabbit test!");
        //}
    }
}

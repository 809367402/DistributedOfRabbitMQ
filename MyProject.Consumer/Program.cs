﻿using log4net;
using RabbitMQ.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace MyProject.Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (GetRunningInstance() == null)
            {
                log4net.Config.XmlConfigurator.Configure(new FileInfo(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\log4net.config"));
                double Time_ProtectInterval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["ConsumerProtectInterval"]);

                Console.WriteLine("************************** 开始启动消费者 ****************************");
                RQConsumer.ConsumerServiceStart();
                Console.WriteLine("************************** 消费者启动成功 ****************************");

                System.Timers.Timer timer = new System.Timers.Timer();
                timer.Enabled = true;
                timer.Interval = Time_ProtectInterval * 60 * 1000;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timerComsumer_Elapsed);
                timer.Start();
            }
        }

        /// <summary>
        /// 获取应用程序进程实例,如果没有匹配进程，返回Null
        /// </summary>
        /// <returns>返回当前Process实例</returns>
        public static Process GetRunningInstance()
        {

            Process currentProcess = Process.GetCurrentProcess();//获取当前进程
            //获取当前运行程序完全限定名
            string currentFileName = currentProcess.MainModule.FileName;
            //获取进程名为ProcessName的Process数组。
            Process[] processes = Process.GetProcessesByName(currentProcess.ProcessName);
            //遍历有相同进程名称正在运行的进程
            foreach (Process process in processes)
            {
                if (process.MainModule.FileName == currentFileName)
                {
                    if (process.Id != currentProcess.Id)//根据进程ID排除当前进程
                        return process;//返回已运行的进程实例
                }
            }
            return null;
        }


        private static readonly ILog log = LogManager.GetLogger(typeof(Program)); 
        static bool IsCheckComplate = true;
        static int Times = 0;
        private static void timerComsumer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            System.Configuration.ConfigurationManager.RefreshSection("appSettings");
            if (IsCheckComplate)
            {
                IsCheckComplate = false;
                try
                {
                    Times += 1;
                    log.InfoFormat("......同步服务消费者订阅第{0}次......", Times.ToString());

                    RQConsumer.ConsumerServiceStart();
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                }
            }
            IsCheckComplate = true;
        }
    }
}

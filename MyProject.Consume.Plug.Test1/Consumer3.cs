using RabbitMQ.Client;
using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyProject.Consume.Plug.Test1
{
    public class Consumer3 : BaseMessageConsumer
    {

        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header, log4net.ILog loger, Func<object, string, string, bool> ErorrMethod)
        {
            try
            {
                object obj = base.ConvertFromMessageToObject(message);
                // Thread.Sleep(11000);


                loger.Info("Consumer3 消费了消息");
                channel.BasicAck(header.DeliveryTag, false);

                //channel.BasicReject(header.DeliveryTag, true);
            }
            catch (Exception ex)
            {
                channel.BasicReject(header.DeliveryTag, true);
                loger.Error(ex);
            }
        }

    }
}

using RabbitMQ.Client;
using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyProject.Consume.Plug.Test1
{
    public class Consumer1: BaseMessageConsumer
    {
       // private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Consumer1));

        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header,log4net.ILog loger, Func<object, string, string, bool> ErorrMethod)
        {
            try
            {
               string result = base.ConvertFromMessageToString(message);
                //Thread.Sleep(100);

                loger.DebugFormat("消费了消息:{0}",result);
                channel.BasicAck(header.DeliveryTag, false);
            }
            catch (Exception ex)
            {
                
                channel.BasicReject(header.DeliveryTag, true);
                loger.Error(ex);
            }
        }
       
    }
}

using RabbitMQ.Client;
using RabbitMQ.Gateway;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MyProject.Consume.Plug.Test1
{
    public class Consumer2: BaseMessageConsumer
    {

        public override void ConsumeMessage(Message message, IModel channel, DeliveryHeader header,log4net.ILog loger,Func<object,string,string,bool> ErorrMethod)
        {
            try
            {
                //object obj = base.ConvertFromMessageToObject(message);
                //// Thread.Sleep(11000);

                ErorrMethod("test", "cin_pub_erorr", "cin_rk_error");
                //loger.Info("Consumer2 消费了消息");
                channel.BasicAck(header.DeliveryTag, false);

                //channel.BasicReject(header.DeliveryTag, true);
            }
            catch (Exception ex)
            {

                channel.BasicReject(header.DeliveryTag, true);
                loger.Error(ex);
            }
        }

    }
}

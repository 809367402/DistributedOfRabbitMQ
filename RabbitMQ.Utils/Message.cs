﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RabbitMQ.Utils
{
    [Serializable]
    public class MessageObj
    {
        /// <summary>
        /// 对象类型
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 对象实例
        /// </summary>
        public object Object { get; set; }
        /// <summary>
        /// 的集合名
        /// </summary>
        public string CollectionName { get; set; }
        /// <summary>
        /// 操作类型
        /// </summary>
        public int OperateType { get; set; }
    }

    /// <summary>
    /// 异常消息体
    /// </summary>
    public class MessageError
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        public string ErrContent
        {
            get;
            set;
        }

        /// <summary>
        /// 消息体
        /// </summary>
        public object MessagBody
        {
            get;
            set;
        }

        /// <summary>
        /// 时间戳
        /// </summary>
        public DateTime Timestamp
        {
            get { return System.DateTime.Now; }
        }
    }


}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RabbitMQ.Gateway;
using System.IO;
using System.Threading;
using BaseRabbitMQ = RabbitMQ.Client;
using RabbitMQ.Gateway.Converter;

namespace RabbitMQ.Utils
{
    /// <summary>
    /// 消息处理及消费
    /// </summary>
    public class MessageProcessor : StringToMessageConverter
    {
        public string result { get; set; }
        //public IExceptionManager EM { get; set; }
        public Exception ex { get; set; }
        public MessageProcessor()//IExceptionManager em
        {
            //EM = em;
        }
        //public string RoutingKey { get; protected set; }
        public MessageProcessor(string routingKey)
        {
          //  this.RoutingKey = routingKey;
        }
        public override Message ConvertObjectToMessage(BaseRabbitMQ.IModel channel, object packetToSend)
        {
            var message = base.ConvertObjectToMessage(channel, packetToSend);
           // message.RoutingKey = this.RoutingKey;//"order.bis.Test";// order.cps.GAP
            return message;
        }
    }
}

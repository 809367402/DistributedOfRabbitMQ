using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections;
namespace RabbitMQ.Gateway.Stubs
{
	public class NullConnection : IConnection, System.IDisposable
	{
		public event CallbackExceptionEventHandler CallbackException;
		public event ConnectionShutdownEventHandler ConnectionShutdown;
		public bool AutoClose
		{
			get
			{
				return true;
			}
            set {
                value = true;
            }
		}
		public ushort ChannelMax
		{
			get
			{
				return 1;
			}
		}
		public System.Collections.IDictionary ClientProperties
		{
			get
			{
				return new System.Collections.Hashtable();
			}
		}
		public ShutdownEventArgs CloseReason
		{
			get
			{
				return new ShutdownEventArgs(ShutdownInitiator.Application, 200, "test");
			}
		}
		public AmqpTcpEndpoint Endpoint
		{
			get
			{
				return new AmqpTcpEndpoint();
			}
		}
		public uint FrameMax
		{
			get
			{
				return 1u;
			}
		}
		public ushort Heartbeat
		{
			get
			{
				return 0;
			}
		}
		public bool IsOpen
		{
			get
			{
				return true;
			}
		}
		public AmqpTcpEndpoint[] KnownHosts
		{
			get
			{
				return null;
			}
		}
		public IProtocol Protocol
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}
		public System.Collections.IDictionary ServerProperties
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}
		public System.Collections.IList ShutdownReport
		{
			get
			{
				throw new System.NotImplementedException();
			}
		}
		public void Abort(ushort reasonCode, string reasonText, int timeout)
		{
		}
		public void Abort(int timeout)
		{
		}
		public void Abort(ushort reasonCode, string reasonText)
		{
		}
		public void Abort()
		{
		}
		public void Close(ushort reasonCode, string reasonText, int timeout)
		{
		}
		public void Close(int timeout)
		{
		}
		public void Close(ushort reasonCode, string reasonText)
		{
		}
		public void Close()
		{
		}
		public IModel CreateModel()
		{
			return new NullModel();
		}
		public void Dispose()
		{
		}
	}
}

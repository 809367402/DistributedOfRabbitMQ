using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections;
namespace RabbitMQ.Gateway.Stubs
{
	public class NullModel : IModel, System.IDisposable
	{
		public event BasicAckEventHandler BasicAcks;
		public event BasicNackEventHandler BasicNacks;
		public event BasicRecoverOkEventHandler BasicRecoverOk;
		public event BasicReturnEventHandler BasicReturn;
		public event CallbackExceptionEventHandler CallbackException;
		public event FlowControlEventHandler FlowControl;
		public event ModelShutdownEventHandler ModelShutdown;
		public ShutdownEventArgs CloseReason
		{
			get
			{
				return new ShutdownEventArgs(ShutdownInitiator.Application, 1, "test");
			}
		}
		public IBasicConsumer DefaultConsumer
		{
			get
			{
				throw new System.NotImplementedException();
			}
			set
			{
				throw new System.NotImplementedException();
			}
		}
		public bool IsOpen
		{
			get
			{
				return true;
			}
		}
		public ulong NextPublishSeqNo
		{
			get
			{
				return 1uL;
			}
		}
		public void Abort(ushort replyCode, string replyText)
		{
		}
		public void Abort()
		{
		}
		public void BasicAck(ulong deliveryTag, bool multiple)
		{
		}
		public void BasicCancel(string consumerTag)
		{
		}
		public string BasicConsume(string queue, bool noAck, string consumerTag, bool noLocal, bool exclusive, System.Collections.IDictionary arguments, IBasicConsumer consumer)
		{
			return "testconsumer";
		}
		public string BasicConsume(string queue, bool noAck, string consumerTag, System.Collections.IDictionary arguments, IBasicConsumer consumer)
		{
			return "testconsumer";
		}
		public string BasicConsume(string queue, bool noAck, string consumerTag, IBasicConsumer consumer)
		{
			return "testconsumer";
		}
		public string BasicConsume(string queue, bool noAck, IBasicConsumer consumer)
		{
			return "testconsumer";
		}
		public BasicGetResult BasicGet(string queue, bool noAck)
		{
			return new BasicGetResult(1uL, false, "test", "test", 0u, null, null);
		}
		public void BasicNack(ulong deliveryTag, bool multiple, bool requeue)
		{
		}
		public void BasicPublish(string exchange, string routingKey, bool mandatory, bool immediate, IBasicProperties basicProperties, byte[] body)
		{
		}
		public void BasicPublish(string exchange, string routingKey, IBasicProperties basicProperties, byte[] body)
		{
		}
		public void BasicPublish(PublicationAddress addr, IBasicProperties basicProperties, byte[] body)
		{
		}
		public void BasicPublish(string exchange, string routingKey, bool mandatory, IBasicProperties basicProperties, byte[] body)
		{
		}
		public void BasicQos(uint prefetchSize, ushort prefetchCount, bool global)
		{
		}
		public void BasicRecover(bool requeue)
		{
		}
		public void BasicRecoverAsync(bool requeue)
		{
		}
		public void BasicReject(ulong deliveryTag, bool requeue)
		{
		}
		public void ChannelFlow(bool active)
		{
		}
		public void Close(ushort replyCode, string replyText)
		{
		}
		public void Close()
		{
		}
		public void ConfirmSelect()
		{
		}
		public IBasicProperties CreateBasicProperties()
		{
			throw new System.NotImplementedException();
		}
		public IFileProperties CreateFileProperties()
		{
			throw new System.NotImplementedException();
		}
		public IStreamProperties CreateStreamProperties()
		{
			throw new System.NotImplementedException();
		}
		public void DtxSelect()
		{
		}
		public void DtxStart(string dtxIdentifier)
		{
		}
		public void ExchangeBind(string destination, string source, string routingKey)
		{
		}
		public void ExchangeBind(string destination, string source, string routingKey, System.Collections.IDictionary arguments)
		{
		}
		public void ExchangeDeclare(string exchange, string type)
		{
		}
		public void ExchangeDeclare(string exchange, string type, bool durable)
		{
		}
		public void ExchangeDeclare(string exchange, string type, bool durable, bool autoDelete, System.Collections.IDictionary arguments)
		{
		}
		public void ExchangeDeclarePassive(string exchange)
		{
		}
		public void ExchangeDelete(string exchange)
		{
		}
		public void ExchangeDelete(string exchange, bool ifUnused)
		{
		}
		public void ExchangeUnbind(string destination, string source, string routingKey)
		{
		}
		public void ExchangeUnbind(string destination, string source, string routingKey, System.Collections.IDictionary arguments)
		{
		}
		public void QueueBind(string queue, string exchange, string routingKey)
		{
		}
		public void QueueBind(string queue, string exchange, string routingKey, System.Collections.IDictionary arguments)
		{
		}
		public QueueDeclareOk QueueDeclare(string queue, bool durable, bool exclusive, bool autoDelete, System.Collections.IDictionary arguments)
		{
			return new QueueDeclareOk("test", 0u, 1u);
		}
		public QueueDeclareOk QueueDeclare()
		{
			return new QueueDeclareOk("test", 0u, 1u);
		}
		public QueueDeclareOk QueueDeclarePassive(string queue)
		{
			return new QueueDeclareOk("test", 0u, 1u);
		}
		public uint QueueDelete(string queue)
		{
			return 0u;
		}
		public uint QueueDelete(string queue, bool ifUnused, bool ifEmpty)
		{
			return 0u;
		}
		public uint QueuePurge(string queue)
		{
			return 0u;
		}
		public void QueueUnbind(string queue, string exchange, string routingKey, System.Collections.IDictionary arguments)
		{
		}
		public void TxCommit()
		{
		}
		public void TxRollback()
		{
		}
		public void TxSelect()
		{
		}
		public bool WaitForConfirms()
		{
			return true;
		}
		public bool WaitForConfirms(System.TimeSpan timeout, out bool timedOut)
		{
			timedOut = false;
			return true;
		}
		public void WaitForConfirmsOrDie()
		{
		}
		public void WaitForConfirmsOrDie(System.TimeSpan timeout)
		{
		}
		public void Dispose()
		{
		}
	}
}

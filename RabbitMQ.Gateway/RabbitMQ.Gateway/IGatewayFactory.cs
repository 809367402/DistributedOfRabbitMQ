using System;
namespace RabbitMQ.Gateway
{
	public interface IGatewayFactory : System.IDisposable
	{
		IAsyncConsumerProcessor GetAsyncReceiver(string ReceiverName, MessageConsumer handler,bool qMode);
		IPublisher GetPublisher(string PublisherName, ConvertToMessage converter,bool ReConncetion);
	}
}

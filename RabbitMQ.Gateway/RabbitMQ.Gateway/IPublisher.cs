using System;
namespace RabbitMQ.Gateway
{
	public interface IPublisher:ICloneable
    {
		string Exchange
		{
			get;
		}
		bool Mandatory
		{
			get;
		}
		bool Immediate
		{
			get;
		}

        bool ChannelIsOpen
        {
            get;
        }

        bool Publish(object ObjectToPublish);

        bool Publish(object ObjectToPublish,string RouteKey);
    }
}

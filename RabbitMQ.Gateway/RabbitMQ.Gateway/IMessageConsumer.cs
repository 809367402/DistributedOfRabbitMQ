using RabbitMQ.Client;
using System;
namespace RabbitMQ.Gateway
{
	public interface IMessageConsumer
	{
		void ConsumeMessage(Message message, IModel channel, DeliveryHeader header,log4net.ILog loger,Func<object,string,string,bool> ErrorMethod);
	}
}

using System;
using System.Runtime.Serialization;
namespace OSGH.Utilities.Common
{
	[Serializable]
	public class FakeException : Exception
	{
		public FakeException(string message) : base(message)
		{
		}
		public FakeException(string message, Exception exception) : base(message, exception)
		{
		}
		public FakeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}

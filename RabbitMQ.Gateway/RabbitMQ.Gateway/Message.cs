using RabbitMQ.Client;
using System;
namespace RabbitMQ.Gateway
{
    [Serializable]
	public class Message
	{
		public IBasicProperties Properties
		{
			get;
			set;
		}
		public byte[] Body
		{
			get;
			set;
		}
		public string RoutingKey
		{
			get;
			set;
		}
	}
}


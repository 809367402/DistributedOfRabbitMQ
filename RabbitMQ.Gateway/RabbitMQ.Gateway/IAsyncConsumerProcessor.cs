using System;
namespace RabbitMQ.Gateway
{
	public interface IAsyncConsumerProcessor
	{
		string Queue
		{
			get;
			set;
		}
		void Stop();

        bool IsOpen
        {
            get; 
        }

        bool Close
        {
            set;
        }
	}
}

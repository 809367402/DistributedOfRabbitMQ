﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RabbitMQ.Gateway
{
    public class Control
    {
        private static bool _isstop = false;
        /// <summary>
        /// 是否停止消费者消费
        /// </summary>
        public static bool IsStopConsumer
        {
            get
            {
                return _isstop;
            }
            set
            {
                value = _isstop;
            }
        }
    }
}

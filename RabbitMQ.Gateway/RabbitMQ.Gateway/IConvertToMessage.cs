using RabbitMQ.Client;
using System;
namespace RabbitMQ.Gateway
{
	public interface IConvertToMessage
	{
		Message ConvertObjectToMessage(IModel channel, object packetToSend);
	}
}

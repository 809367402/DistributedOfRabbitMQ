using System;
namespace RabbitMQ.Gateway
{
	public class DeliveryHeader
	{
		public bool Redelivered
		{
			get;
			internal set;
		}
		public ulong DeliveryTag
		{
			get;
			internal set;
		}
	}
}

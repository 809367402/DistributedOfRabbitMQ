using System;
using System.IO;
using System.Text;
namespace RabbitMQ.Gateway.Converter
{
	public static class MessageToStringConverter
	{
		public static string Convert(Message message)
		{
			string result = string.Empty;
			if (message.Properties.ContentType == Const.PLAIN_TEXT)
			{
				System.Text.Encoding encoding = System.Text.Encoding.GetEncoding(message.Properties.ContentEncoding ?? "utf-8");
				System.IO.MemoryStream stream = new System.IO.MemoryStream(message.Body);
				System.IO.StreamReader streamReader = new System.IO.StreamReader(stream, encoding, false);
				result = streamReader.ReadToEnd();
			}
			return result;
		}
	}
}

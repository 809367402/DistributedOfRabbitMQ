﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RabbitMQ.Gateway
{
    public class Const
    {
        /// <summary>
        /// 消息类型-文本
        /// </summary>
        public static readonly string PLAIN_TEXT = "text/plain";

        /// <summary>
        /// 消息类型-对象
        /// </summary>
        public static readonly string PlAIN_OBJ = "text/object";
    }
}

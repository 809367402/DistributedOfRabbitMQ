﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fastJSON;

namespace RabbitMQ.Gateway
{
    public class JsonUtils
    {
        public static string ToJSON(object o)
        {
            string result = string.Empty;

            fastJSON.JSON.Instance.Parameters.UseExtensions = false;
            fastJSON.JSON.Instance.Parameters.UseEscapedUnicode = false;

            result = fastJSON.JSON.Instance.ToJSON(o);

            return result;
        }

        public static string ToJSON(Object o, JSONParameters jp)
        {
            string result = string.Empty;

            result = fastJSON.JSON.Instance.ToJSON(o, jp);

            return result;
        }

        public static T ToObject<T>(string s)
        {
            T t = default(T);
            t = fastJSON.JSON.Instance.ToObject<T>(s);
            return t;
        }
    }
}

using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class BindingCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public Binding this[int index]
		{
			get
			{
				return (Binding)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new Binding this[string Name]
		{
			get
			{
				return (Binding)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new Binding();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
			Binding binding = (Binding)element;
			return BindingCollection.CreateKey(binding);
		}
		private static string CreateKey(Binding binding)
		{
            return string.Join(",", new string[]
            {
                binding.Exchange,
                binding.Queue,
                binding.Connection,
				binding.SuscriptionKey
			});
		}
		public int IndexOf(Binding binding)
		{
			return base.BaseIndexOf(binding);
		}
		public void Add(Binding binding)
		{
			this.BaseAdd(binding);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(Binding binding)
		{
			if (base.BaseIndexOf(binding) >= 0)
			{
				base.BaseRemove(BindingCollection.CreateKey(binding));
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

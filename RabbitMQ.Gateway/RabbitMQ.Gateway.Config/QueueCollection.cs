using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class QueueCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public Queue this[int index]
		{
			get
			{
				return (Queue)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new Queue this[string Name]
		{
			get
			{
				return (Queue)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new Queue();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
            Queue Queue = (Queue)element;
            return QueueCollection.CreateKey(Queue);
            //return ((Queue)element).Name;
        }
        private static string CreateKey(Queue Queue)
        {
            return string.Join(",", new string[]
            {
                Queue.Name,
                Queue.Connection,
            });
        }

        public int IndexOf(Queue queue)
		{
			return base.BaseIndexOf(queue);
		}
		public void Add(Queue queue)
		{
			this.BaseAdd(queue);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(Queue queue)
		{
			if (base.BaseIndexOf(queue) >= 0)
			{
				base.BaseRemove(queue.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class ExchangeCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public Exchange this[int index]
		{
			get
			{
				return (Exchange)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new Exchange this[string Name]
		{
			get
			{
				return (Exchange)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new Exchange();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
            Exchange Exchange = (Exchange)element;
            return ExchangeCollection.CreateKey(Exchange);
        }

        private static string CreateKey(Exchange Exchange)
        {
            return string.Join(",", new string[]
            {
                Exchange.Name,
                Exchange.Connection,
            });
        }
        public int IndexOf(Exchange exchange)
		{
			return base.BaseIndexOf(exchange);
		}
		public void Add(Exchange exchange)
		{
			this.BaseAdd(exchange);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(Exchange exchange)
		{
			if (base.BaseIndexOf(exchange) >= 0)
			{
				base.BaseRemove(exchange.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

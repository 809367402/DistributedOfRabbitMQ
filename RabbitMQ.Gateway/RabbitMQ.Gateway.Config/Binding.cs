using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class Binding : ConfigurationElement
	{
		[ConfigurationProperty("exchange", DefaultValue = "", IsRequired = true)]
		public string Exchange
		{
			get
			{
				return (string)base["exchange"];
			}
			set
			{
				base["exchange"] = value;
			}
		}
		[ConfigurationProperty("connection", DefaultValue = "", IsRequired = true)]
		public string Connection
		{
			get
			{
				return (string)base["connection"];
			}
			set
			{
				base["connection"] = value;
			}
		}
		[ConfigurationProperty("queue", DefaultValue = "", IsRequired = true)]
		public string Queue
		{
			get
			{
				return (string)base["queue"];
			}
			set
			{
				base["queue"] = value;
			}
		}
		[ConfigurationProperty("subscriptionkey", DefaultValue = "", IsRequired = true)]
		public string SuscriptionKey
		{
			get
			{
				return (string)base["subscriptionkey"];
			}
			set
			{
				base["subscriptionkey"] = value;
			}
		}
	}
}

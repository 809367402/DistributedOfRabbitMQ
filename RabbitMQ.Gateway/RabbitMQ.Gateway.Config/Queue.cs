using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class Queue : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string)base["name"];
			}
			set
			{
				base["name"] = value;
			}
		}
		[ConfigurationProperty("connection", DefaultValue = "", IsRequired = true)]
		public string Connection
		{
			get
			{
				return (string)base["connection"];
			}
			set
			{
				base["connection"] = value;
			}
		}
		[ConfigurationProperty("autodelete", DefaultValue = true, IsRequired = false)]
		public bool AutoDelete
		{
			get
			{
				return (bool)base["autodelete"];
			}
			set
			{
				base["autodelete"] = value;
			}
		}
		[ConfigurationProperty("durable", DefaultValue = false, IsRequired = false)]
		public bool Durable
		{
			get
			{
				return (bool)base["durable"];
			}
			set
			{
				base["durable"] = value;
			}
		}
		[ConfigurationProperty("exclusive", DefaultValue = false, IsRequired = false)]
		public bool Exclusive
		{
			get
			{
				return (bool)base["exclusive"];
			}
			set
			{
				base["exclusive"] = value;
			}
		}
	}
}

using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
    public class AsyncReceiver : ConfigurationElement
    {
        [ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)base["name"];
            }
            set
            {
                base["name"] = value;
            }
        }
        [ConfigurationProperty("connection", DefaultValue = "", IsRequired = true)]
        public string Connection
        {
            get
            {
                return (string)base["connection"];
            }
            set
            {
                base["connection"] = value;
            }
        }
        [ConfigurationProperty("queue", DefaultValue = "", IsRequired = true)]
        public string Queue
        {
            get
            {
                return (string)base["queue"];
            }
            set
            {
                base["queue"] = value;
            }
        }

        [ConfigurationProperty("assembly", DefaultValue = "", IsRequired = true)]
        public string Assembly
        {
            get
            {
                return (string)base["assembly"];
            }
            set
            {
                base["assembly"] = value;
            }
        }

        [ConfigurationProperty("class", DefaultValue = "", IsRequired = true)]
        public string Class
        {
            get
            {
                return (string)base["class"];
            }
            set
            {
                base["class"] = value;
            }
        }

        [ConfigurationProperty("maxthreads", DefaultValue = 1, IsRequired = false)]
        public int MaxThreads
        {
            get
            {
                return (int)base["maxthreads"];
            }
            set
            {
                base["maxthreads"] = value;
            }
        }

        [ConfigurationProperty("logername", DefaultValue = "root", IsRequired = false)]
        public string LogerName
        {
            get
            {
                return (string)base["logername"];
            }
            set
            {
                base["logername"] = value;
            }
        }
	}
}

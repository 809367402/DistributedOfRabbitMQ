using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class AsyncReceiverCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public AsyncReceiver this[int index]
		{
			get
			{
				return (AsyncReceiver)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new AsyncReceiver this[string Name]
		{
			get
			{
				return (AsyncReceiver)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new AsyncReceiver();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((AsyncReceiver)element).Name;
		}
		public int IndexOf(AsyncReceiver asyncReceiver)
		{
			return base.BaseIndexOf(asyncReceiver);
		}
		public void Add(AsyncReceiver asyncReceiver)
		{
			this.BaseAdd(asyncReceiver);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(AsyncReceiver asyncReceiver)
		{
			if (base.BaseIndexOf(asyncReceiver) >= 0)
			{
				base.BaseRemove(asyncReceiver.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class PublisherCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public Publisher this[int index]
		{
			get
			{
				return (Publisher)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new Publisher this[string Name]
		{
			get
			{
				return (Publisher)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new Publisher();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((Publisher)element).Name;
		}
		public int IndexOf(Publisher publisher)
		{
			return base.BaseIndexOf(publisher);
		}
		public void Add(Publisher publisher)
		{
			this.BaseAdd(publisher);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(Publisher publisher)
		{
			if (base.BaseIndexOf(publisher) >= 0)
			{
				base.BaseRemove(publisher.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

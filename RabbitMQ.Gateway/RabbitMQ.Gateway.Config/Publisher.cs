using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class Publisher : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string)base["name"];
			}
			set
			{
				base["name"] = value;
			}
		}
		[ConfigurationProperty("connection", DefaultValue = "", IsRequired = true)]
		public string Connection
		{
			get
			{
				return (string)base["connection"];
			}
			set
			{
				base["connection"] = value;
			}
		}

		[ConfigurationProperty("exchange", DefaultValue = "", IsRequired = true)]
		public string Exchange
		{
			get
			{
				return (string)base["exchange"];
			}
			set
			{
				base["exchange"] = value;
			}
		}

        [ConfigurationProperty("routekey", DefaultValue = "", IsRequired = false)]
        public string RouteKey
        {
            get
            {
                return (string)base["routekey"];
            }
            set
            {
                base["routekey"] = value;
            }
        }

		[ConfigurationProperty("mandatory", DefaultValue = false, IsRequired = false)]
		public bool Mandatory
		{
			get
			{
				return (bool)base["mandatory"];
			}
			set
			{
				base["mandatory"] = value;
			}
		}
		[ConfigurationProperty("immediate", DefaultValue = false, IsRequired = false)]
		public bool Immediate
		{
			get
			{
				return (bool)base["immediate"];
			}
			set
			{
				base["immediate"] = value;
			}
		}
	}
}

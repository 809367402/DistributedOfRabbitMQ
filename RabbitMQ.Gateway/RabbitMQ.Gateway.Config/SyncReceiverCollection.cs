using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class SyncReceiverCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public SyncReceiver this[int index]
		{
			get
			{
				return (SyncReceiver)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new SyncReceiver this[string Name]
		{
			get
			{
				return (SyncReceiver)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new SyncReceiver();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((SyncReceiver)element).Name;
		}
		public int IndexOf(SyncReceiver syncReceiver)
		{
			return base.BaseIndexOf(syncReceiver);
		}
		public void Add(SyncReceiver syncReceiver)
		{
			this.BaseAdd(syncReceiver);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(SyncReceiver syncReceiver)
		{
			if (base.BaseIndexOf(syncReceiver) >= 0)
			{
				base.BaseRemove(syncReceiver.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

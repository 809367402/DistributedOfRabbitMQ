using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class ConnectionFactory : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string)base["name"];
			}
			set
			{
				base["name"] = value;
			}
		}
		[ConfigurationProperty("server", DefaultValue = "", IsRequired = true)]
		public string Server
		{
			get
			{
				return (string)base["server"];
			}
			set
			{
				base["server"] = value;
			}
		}
		[ConfigurationProperty("host", DefaultValue = "", IsRequired = true)]
		public string Host
		{
			get
			{
				return (string)base["host"];
			}
			set
			{
				base["host"] = value;
			}
		}
		[ConfigurationProperty("port", DefaultValue = 5672, IsRequired = true)]
		public int Port
		{
			get
			{
				return (int)base["port"];
			}
			set
			{
				base["port"] = value;
			}
		}
		[ConfigurationProperty("vhost", DefaultValue = "v_mq", IsRequired = true)]
		public string VHost
		{
			get
			{
				return (string)base["vhost"];
			}
			set
			{
				base["vhost"] = value;
			}
		}
		[ConfigurationProperty("username", DefaultValue = "guest", IsRequired = false)]
		public string UserName
		{
			get
			{
				return (string)base["username"];
			}
			set
			{
				base["username"] = value;
			}
		}
		[ConfigurationProperty("password", DefaultValue = "guest", IsRequired = false)]
		public string Password
		{
			get
			{
				return (string)base["password"];
			}
			set
			{
				base["password"] = value;
			}
		}
	}
}

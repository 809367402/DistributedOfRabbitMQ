using System;
namespace RabbitMQ.Gateway.Config
{
	public enum ExchangeType
	{
		direct,
		topic,
		fanout
	}
}

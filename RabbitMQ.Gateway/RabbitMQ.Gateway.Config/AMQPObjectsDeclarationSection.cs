using System;
using System.Configuration;
using System.Xml;
namespace RabbitMQ.Gateway.Config
{
	public class AMQPObjectsDeclarationSection : ConfigurationSection
	{
		[ConfigurationProperty("ExchangeList", IsDefaultCollection = false)]
		public ExchangeCollection ExchangeList
		{
			get
			{
				return (ExchangeCollection)base["ExchangeList"];
			}
		}
		[ConfigurationProperty("QueueList", IsDefaultCollection = false)]
		public QueueCollection QueueList
		{
			get
			{
				return (QueueCollection)base["QueueList"];
			}
		}
		[ConfigurationProperty("BindingList", IsDefaultCollection = false)]
		public BindingCollection BindingList
		{
			get
			{
				return (BindingCollection)base["BindingList"];
			}
		}
		protected override void DeserializeSection(XmlReader reader)
		{
			base.DeserializeSection(reader);
		}
		protected override string SerializeSection(ConfigurationElement parentElement, string name, ConfigurationSaveMode saveMode)
		{
			return base.SerializeSection(parentElement, name, saveMode);
		}
	}
}

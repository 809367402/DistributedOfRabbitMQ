using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class SyncReceiver : ConfigurationElement
	{
		[ConfigurationProperty("name", DefaultValue = "", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string)base["name"];
			}
			set
			{
				base["name"] = value;
			}
		}
		[ConfigurationProperty("connection", DefaultValue = "", IsRequired = true)]
		public string Connection
		{
			get
			{
				return (string)base["connection"];
			}
			set
			{
				base["connection"] = value;
			}
		}
		[ConfigurationProperty("queue", DefaultValue = "", IsRequired = true)]
		public string Queue
		{
			get
			{
				return (string)base["queue"];
			}
			set
			{
				base["queue"] = value;
			}
		}
	}
}

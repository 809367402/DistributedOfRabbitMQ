using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
	public class ConnectionCollection : ConfigurationElementCollection
	{
		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.AddRemoveClearMap;
			}
		}
		public new string AddElementName
		{
			get
			{
				return base.AddElementName;
			}
			set
			{
				base.AddElementName = value;
			}
		}
		public new string ClearElementName
		{
			get
			{
				return base.ClearElementName;
			}
			set
			{
				base.ClearElementName = value;
			}
		}
		public new string RemoveElementName
		{
			get
			{
				return base.RemoveElementName;
			}
		}
		public new int Count
		{
			get
			{
				return base.Count;
			}
		}
		public ConnectionFactory this[int index]
		{
			get
			{
				return (ConnectionFactory)base.BaseGet(index);
			}
			set
			{
				if (base.BaseGet(index) != null)
				{
					base.BaseRemoveAt(index);
				}
				this.BaseAdd(index, value);
			}
		}
		public new ConnectionFactory this[string Name]
		{
			get
			{
				return (ConnectionFactory)base.BaseGet(Name);
			}
		}
		protected override ConfigurationElement CreateNewElement()
		{
			return new ConnectionFactory();
		}
		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((ConnectionFactory)element).Name;
		}
		public int IndexOf(ConnectionFactory connectionFactory)
		{
			return base.BaseIndexOf(connectionFactory);
		}
		public void Add(ConnectionFactory connectionFactory)
		{
			this.BaseAdd(connectionFactory);
		}
		protected override void BaseAdd(ConfigurationElement element)
		{
			base.BaseAdd(element, false);
		}
		public void Remove(ConnectionFactory connectionFactory)
		{
			if (base.BaseIndexOf(connectionFactory) >= 0)
			{
				base.BaseRemove(connectionFactory.Name);
			}
		}
		public void RemoveAt(int index)
		{
			base.BaseRemoveAt(index);
		}
		public void Remove(string name)
		{
			base.BaseRemove(name);
		}
		public void Clear()
		{
			base.BaseClear();
		}
	}
}

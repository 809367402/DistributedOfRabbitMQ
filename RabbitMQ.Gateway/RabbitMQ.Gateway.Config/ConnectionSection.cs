using System;
using System.Configuration;
namespace RabbitMQ.Gateway.Config
{
    /// <summary>
    /// ����
    /// </summary>
	public class ConnectionSection : ConfigurationSection
    {
        [ConfigurationProperty("ConnectionList", IsRequired = true)]
        public ConnectionCollection ConnectionList
        {
            get
            {
                return (ConnectionCollection)base["ConnectionList"];
            }
        }
        [ConfigurationProperty("PublisherList", IsRequired = false)]
        public PublisherCollection PublisherList
        {
            get
            {
                return (PublisherCollection)base["PublisherList"];
            }
        }

		[ConfigurationProperty("SyncReceiverList", IsRequired = false)]
		public SyncReceiverCollection SyncReceiverList
		{
			get
			{
				return (SyncReceiverCollection)base["SyncReceiverList"];
			}
		}
		[ConfigurationProperty("AsyncReceiverList", IsRequired = false)]
		public AsyncReceiverCollection AsyncReceiverList
		{
			get
			{
				return (AsyncReceiverCollection)base["AsyncReceiverList"];
			}
		}
	}
}
